# What this repo contains

An implementation of the _k-means clustering_ algorithm and a set of visualizations intended to demonstrate its use

The algorithm uses "random partition" initialization: each observation is assigned a random initial cluster, and then the initial cluster locations are calculated as the centroid of their assigned observations. This is as opposed to "Forgy" initialization, where clusters are initialized to random observations of the dataset.

# Installation

1. Clone the repo to some folder
2. Open `code/index.html` in a web browser (tested in firefox and chrome).

# Usage

This application has two major stages -- selecting data and then the clustering itself.

## Selecting Data

There are two methods of selecting data.

### GUI input

The GUI input constitutes a simple way of creating 2-dimensional clusters. **Click the scatterplot to add clusters of points**. These clusters are centered in a 2d gaussian distribution around the click point, with variance controlled with the **locational spread of points** input beneath the chart. A variance of 1 will make points between 0 and 1 unit away from the click point, a variance of 15 will make points between 0 and 15 units away from the click point, and so on. The number of points is controlled by the **number of points created on click** input beneath the chart.

Each time you click, the **number of means** input value will increment by one (since each click creates a "cluster"). This value can be changed manually if desired.

Once the desired points are added, click the **next** button to continue to the clustering stage (only visible when points exist).

### CSV input

The CSV input constitutes a way of importing more complex data into the algorithm. Click the **choose file** button to upload a CSV file. Once uploaded, the page will populate with information about the data. At the bottom of the page is the **number of means** input.

The `data` folder contains a subset of the famous iris ML dataset, `iris.csv`. This subset contains 50 observations each of the _setosa_, _versicolor_ and _virginica_ species. When run on this dataset with a _k_ of 3, this implementation has been observed to correctly identify the _setosa_ species with no errors (50/50 _true positives_, 0/100 _false positives_), however it has trouble distinguishing the _versicolor_ and _virginica_ species.

Once your data has been uploaded, click the **next** button to continue to the clustering stage (only visible when data has been uploaded).

## Visualizing the algorithm

The visualization stage contains three major elements -- a console, a parallel-coordinates chart, and a scatterplot. The plots display the data from the previous step, while the console displays information from the algorithm -- which steps have been run, and whether an error has occurred or the means have converged to a local minimum.

The two graphs display both the data and the means themselves. The data is colored according to their currently assigned cluster-mean. On the scatterplot, the cluster-means themselves are represented by larger dots with the same colors as the data but with a black outline. On the parallel-coordinates chart means are represented as thick black lines.

The parallel-coordinates and scatterplot are linked such that the first two dimensions of the parallel-coordinates chart control the _x_ and _y_ axes of the scatterplot. To re-arrange the axes of the parallel-coordinates chart, click and drag the axis labels. This works with categorical dimensions as well, however when categorical dimensions are being displayed on the scatterplot the mean points will not be displayed. Similarly the mean-lines on the parallel-coordinates chart are not drawn on categorical dimensions.

The parallel-coordinates chart has the additional functionality of "brushing," where the user drags a selection brush down an axis. This will highlight observations which fall inside of the range of that brush on that axis. Multiple brushes can be active at a time (though only one per axis), and their effects stack. This does not work on categorical dimensions.

Three controls exist on this page.

- **Run until converged** runs the algorithm until all means converge or 1000 steps pass. An edge case exists where a mean is assigned no observations at which point the algorithm resets using the above described random partition initialization method. Therefore it is possible that the algorithm will repeatedly reset (depending on initial mean placement) and never for all practical purposes converge. However, I have rarely seen this case (except where the number of means is larger than the number of observations, which is illogical anyhow) so the limit of 1000 is mostly a precaution against the annoyance of freezing your browser indefinitely.
- **Step** runs one step of the k-means algorithm. This consists of the assignment step, where each observation is assigned the cluster of the nearest mean, and then the update step, where the means are moved to the centroids of their newly assigned observations. The 0-observation-mean case mentioned above is logged to the console if it occurs, as is convergence to a local minimum.
- **Reset means** runs the random partition initialization step, resetting all observations to random means and running the update step as described above. This is useful for seeing the different possible local minimums of a dataset.
