// via https://stackoverflow.com/questions/17922557/angularjs-how-to-check-for-changes-in-file-input-fields
angular.module('theDriver').directive('customOnChange', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var onChangeHandler = scope.$eval(attrs.customOnChange);
            element.bind('change', onChangeHandler);
        }
    };
});
