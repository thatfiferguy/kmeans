angular.module('theDriver', []).controller('mainController', function($scope) {

    //-----------------------------------------------------
    //---------------------VARIABLES-----------------------
    //-----------------------------------------------------

    $scope.visibilities = {
        "inputselect": true, // select from csv and GUI input
        "input": {
            // "any": false,
            "csv": false,
            "gui": false
        },
        "visualizations": false
    };

    $scope.data = []; //the data
    $scope.means = []; //the k-means

    // the K in K-means
    $scope.nummeans = {cnt: 0}; //becaue angular is bad at watching primatives
    $scope.guicontrols = { //controls for the gui input
        "numpts": 30, //num pts to add on click
        "spread": 30 //variance of gaussian distribution of point locations added on click
    };

    $scope.meanDimensions = ["foo"];

    /**
    * Tells visualizations to redraw their data (only)
    */
    $scope.redrawData = function() {
        console.log("broadcasting a redraw command");
        $scope.$broadcast("redrawData");
    };

    $scope.displaytoggleText = function() {
        return $scope.displaytoggle.cluster ? "cluster value" : "cluster reference value";
    };

    //-----------------------------------------------------
    //-------------------BUTTONS---------------------------
    //-----------------------------------------------------

    /**
    * On clicking the input select button. Decides between gui and csv input visibilities.
    * @param  {boolean} gui did they select the gui select button?
    */
    $scope.inputSelect = function(gui) {

        if(gui) {
            $scope.visibilities.input.gui = true;
            $scope.meanDimensions = ["x", "y"]; //the dimensions to consider when calculating means
        } else {
            $scope.visibilities.input.csv = true;
            $scope.nummeans.cnt++; //at least one mean
            $scope.meanDimensions = ["foo"]; //the dimensions to consider when calculating means
        }

        // $scope.visibilities.input.any = true;
        $scope.visibilities.inputselect = false;
    };

    /**
    * On clicking the "next" button. Moves along visibilities track, prepares for k-means.
    * Also can be used to reset k-means values.
    */
    $scope.goToVisualizations = function() {
        $scope.visibilities.input.csv = false;
        $scope.visibilities.input.gui = false;
        $scope.visibilities.visualizations = true;

        // prepare data -- assign random mean between 1 and k-1 to each data point.
        // also set all number fields to be numbers
        // this is the random partition method
        $scope.data.forEach(function(datum) {
            $scope.meanDimensions.forEach(function(dim) {
                datum[dim] = parseFloat(datum[dim]); //convert to number
            });
            datum.cluster = Math.floor(Math.random() * $scope.nummeans.cnt);
            delete datum.clusterRef; //no longer useful
        });

        //clear console
        document.getElementById('consoleBody').innerHTML = "";


        //generate means
        $scope.updateStep();
    };

    $scope.updateVisualizations = function() {
        setTimeout(function(){
            $scope.$apply(); //apply all updates
            $scope.$broadcast("updateVisualizations"); //tell visualizations to redraw now that kmeans has been run
        }, 125);
    };

    //-----------------------------------------------------
    //-------------------K-MEANS---------------------------
    //-----------------------------------------------------

    /**
    * Appends the given message to the in-window console
    * @param  {string} text the msg to append
    * @param {int} type     if 0, normal msg, if 1, error. if 2, success
    */
    var systemMessage = function(text, type) {
        var body = document.getElementById('consoleBody');

        var style = "";
        if (type === 1) style = "style='color: rgb(131, 42, 55); font-weight: bolder' ";
        else if (type === 2) style = "style='color: rgb(55, 131, 42); font-weight: bolder' ";

        body.insertAdjacentHTML('beforeend', '<div ' + style + 'class="systemmsg">' + text + '</div><br>');
        body.scrollTop = body.scrollHeight;
    };

    /**
    * Assign each data point's cluster to the closest mean
    */
    $scope.assignmentStep = function() {
        systemMessage("assignment step", 0);

        /**
        * Given distances, get the eucldian distance (hypot).
        * adapted from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/hypot
        * @param  {array} coords the distances for each dimension
        * @return {double}       the distance
        */
        function getDist(coords) {
            var y = 0;

            for (var i = 0; i < coords.length; i++) {
                if (coords[i] === Infinity || coords[i] === -Infinity) {
                    return Infinity;
                }
                y += coords[i] * coords[i];
            }
            return Math.sqrt(y);
        }

        $scope.data.forEach(function(datum) {
            //find best mean
            var bestMean = -1;
            var minDist = Number.MAX_VALUE;
            $scope.means.forEach(function(mean) {
                // calc distances for each dimension into array
                dists = [];
                $scope.meanDimensions.forEach(function(dim) {
                    dists.push(Math.abs(datum[dim] - mean[dim]));
                });
                eDist = getDist(dists);
                if (eDist < minDist) {
                    minDist = eDist;
                    bestMean = mean.cluster;
                }
            });
            datum.cluster = bestMean;
        });
    };

    /**
    * Create k new means at the centroids of the observations of this mean
    * @return {int} 0 if no interesting update, 1 if a mean has no observations, 2 if converged
    */
    $scope.updateStep = function() {
        systemMessage("update step", 0);

        var returnstate = 0;
        var oldmeans = $scope.means;
        $scope.means = []; //reset means
        var convergedCnt = 0; //number of converged means
        for (var i = 0; i < $scope.nummeans.cnt; i++) { //for each mean
            // console.log("creating mean");
            //find centroid of each dimension
            var mean = {};
            mean.obsCnt = 0; //number of obsercations at this mean
            mean.cluster = i;
            // mean.clusterRef = i; //otherwise PC flips out
            $scope.meanDimensions.forEach(function(dim) {
                mean[dim] = 0;
            });
            $scope.data.forEach(function(datum) {
                if (datum.cluster == i) { //observation corresponds to this cluster
                    mean.obsCnt++; //count this observation
                    $scope.meanDimensions.forEach(function(dim) {
                        mean[dim] += datum[dim]; // add data of this dimension to mean location on this dimension
                    });
                }
            });

            if (mean.obsCnt === 0) { //no observations for this mean
                $scope.meanDimensions.forEach(function(dim) { // average
                    console.log("no observations mean");
                    mean[dim] = oldmeans[i][dim]
                    returnstate = 1;
                });
                systemMessage("Mean has no observations. Please reset and try again with better initial observations.", 1);
            } else { //mean has observations
                $scope.meanDimensions.forEach(function(dim) { // average
                    mean[dim] = mean[dim] / mean.obsCnt;
                });
            }
            // check if mean has converged
            if (oldmeans.length > 0) { //if old means exist (this is not first step)
                var same = true;
                $scope.meanDimensions.forEach(function(dim) {
                    if (mean[dim] !== oldmeans[i][dim]) same = false;
                });
                if (same) convergedCnt++;
            }
            $scope.means[i] = mean;
        }
        // console.log($scope.means);

        if (convergedCnt === $scope.nummeans.cnt) {
            systemMessage("Means have converged",2);
            if (returnstate != 1) returnstate = 2;
        }

        return returnstate;
    };



    /**
    * Does one step of the k-mean algorithm
    */
    $scope.kmeanStep = function() {
        $scope.assignmentStep();
        $scope.updateStep();
        $scope.updateVisualizations();
    };

    /**
     * Runs k-means until converged or times-out (too many resets)
     */
    $scope.kmeans = function() {
        var resets = 0;
        for (var i = 0; i < 1000; i++) {
            $scope.assignmentStep();
            var state = $scope.updateStep();
            // console.log(state);
            if (state == 1) {
                resets++;
                $scope.goToVisualizations();
            } else if (state == 2) {
                systemMessage("took " + (i+1-resets) + " iterations, " + resets + " resets to converge", 2);
                $scope.updateVisualizations();
                return;
            }
        }


        systemMessage("did not converge after 1000 tries... aborting.", 1);
    };
});
