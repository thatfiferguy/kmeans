angular.module("theDriver").component("csvInput", {
    template: `
    <div id="csvInput"><input id="fileinput" type="file" custom-on-change="uploadFile"></div>
    <div id="stats"></div>
    <div><table id="datatable"></table></div>
    <div id="csvinputcontrols">
    <table>
    <tr>
    </tr>
    </table>
    </div>`,
    bindings: {
        data: '=',
        quantfields: "="
    }, //refer to with $scope.$ctrl.data
    restrict: "E",
    controller: function($scope) {
        function load_dataset(csv) {
            // console.log("loading dataset", csv);
            $scope.$ctrl.data = d3.csv.parse(csv);
            create_table($scope.$ctrl.data);
        }

        function create_table(data) {
            //determine which fields are quantitative

            var keys = d3.keys(data[0]);
            // console.log($scope.$ctrl.meanDimensions);

            $scope.$ctrl.quantfields = [];
            keys.forEach(function(key) {
                var quant = true;
                data.forEach(function(d) {
                    //check if data is quant on this key
                    if (isNaN(d[key])) quant = false;
                });
                if (quant) $scope.$ctrl.quantfields.push(key);
            });

            console.log($scope.$ctrl.quantfields);

            $scope.$apply(); //push changes back up to parent
            // table stats

            var stats = d3.select("#stats")
            .html(`<table>
                    <tr>
                        <td class="inputpair_label">Dimensions:</td>
                        <td class="inputpair_input">`+keys.length+`</td>
                    </tr>
                    <tr>
                        <td class="inputpair_label">Observations:</td>
                        <td class="inputpair_input">`+data.length+`</td>
                    </tr>
                </table>
                <p>Green columns (quantitative data) will be used in clustering.</p>
                <p>Red columns (qualitative or noisy data) will not, but will still be displayed on visualizations.</p>`);

            // stats.append("div")
            // .text();
            //
            // stats.append("div")
            // .text("Dimensions: " + keys.length + " | Observations: " + data.length);
            //
            // stats.append("div")
            // .text("Green columns (quantitative data) will be used in clustering.");
            // stats.append("div")
            // .text(
            //  "Red columns (qualitative or noisy data) will not, but will still be displayed on visualizations.");

            d3.select("#datatable")
            .html("")
            .append("tr")
            .attr("class", "fixed")
            .selectAll("th")
            .data(keys)
            .enter().append("th")
            .text(function(d) {
                return d;
            }).style("background", function(d) {
                return $scope.$ctrl.quantfields.includes(d) ? "rgb(192, 255, 173)" : "rgb(255, 177, 177)";
            });

            d3.select("#datatable")
            .selectAll("tr.row")
            .data(data)
            .enter().append("tr")
            .attr("class", "row")
            .selectAll("td")
            .data(function(d) {
                return keys.map(function(key) {
                    return d[key];
                });
            })
            .enter().append("td")
            .text(function(d) {
                return d;
            })
            .style("border", "1px solid rgb(198, 198, 198)");
        }

        $scope.uploadFile = function(event) {
            var file = event.target.files;
            var reader = new FileReader();
            reader.readAsText(file[0]);
            reader.onload = function() {
                console.log("file read");
                load_dataset(reader.result);
            };
        };
    } //end controller
});
