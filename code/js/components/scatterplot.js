angular.module("theDriver").component("scatterplot",{
    template: '<span id="scatterplotreplace">this will be replaced with the scatterplot</span>',
        bindings: {
            data: '=', //the data
            means: "=", //the means
            fields: "=" //the fields to display
        }, //refer to with $scope.$ctrl.data
    restrict: "E",
    controller: function ($scope) {
        var dimensions;
        var buildSVG = function() {
            console.log("building scatterplot with x, y of", dimensions[0], dimensions[1]);

            var margin = {top: 40, right: 40, bottom: 60, left: 80},
            width = 500 - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;

            //determine if field is categorical
            xCategorical = false;
            yCategorical = false;
            $scope.$ctrl.data.forEach(function(datum) {
                if (isNaN(datum[dimensions[0]])) xCategorical = true;
                if (isNaN(datum[dimensions[1]])) yCategorical = true;
            });

            var x = xCategorical ?
            d3.scale.ordinal().rangePoints([0, width],1)  : d3.scale.linear().range([0, width]);

            var y = yCategorical ?
            d3.scale.ordinal().rangePoints([height, 0],1) : d3.scale.linear().range([height, 0]);

            var color = d3.scale.category20();

            var xAxis = d3.svg.axis()
            .scale(x)
            .tickSize(1)
            .ticks(4) //number of markings
            .orient("bottom");

            var yAxis = d3.svg.axis()
            .scale(y)
            .tickSize(1)
            .ticks(4) //number of markings
            .orient("left");

            // remove previous SVG or content
            document.getElementById("scatterplotreplace").innerHTML = "";

            var svg = d3.select("#scatterplotreplace").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            // get new copy of data
            var data = $scope.$ctrl.data;
            xValue = function(d) { return d[dimensions[0]]; };
            yValue = function(d) { return d[dimensions[1]]; };

            if (xCategorical)
                x.domain(data.map(function(d) { return d[dimensions[0]]; }));
            else
                x.domain([d3.min(data, xValue)-1, d3.max(data, xValue)+1]).nice();
            if (yCategorical)
                y.domain(data.map(function(d) { return d[dimensions[1]]; }));
            else
                y.domain([d3.min(data, yValue)-1, d3.max(data, yValue)+1]).nice();

            svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .append("text")
            .attr("class", "label")
            .attr("x", width)
            .attr("y", -6)
            .style("text-anchor", "end")
            .text(function(d) { return dimensions[0]; }); // label


            svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(function(d) { return dimensions[1]; }); //label

            svg.selectAll(".otherDot")
            .data($scope.$ctrl.data)
            .enter().append("circle")
            .attr("class", "dot")
            .attr("r", 3.5)
            .style('opacity', 0.5)
            .attr("cx", function(d) { return x(d[dimensions[0]]); })
            .attr("cy", function(d) { return y(d[dimensions[1]]); })
            .style("fill", function(d) { return color(d.cluster); });

            // console.log($scope.$ctrl);
            // console.log($scope.$ctrl.means);
            if (!xCategorical && !yCategorical) { //only display means if both axes are quantiative.
                svg.selectAll(".meanDot")
                .data($scope.$ctrl.means)
                .enter().append("circle")
                .attr("class", "meanDot")
                .attr("r", 5)
                .style('opacity', 1)
                .attr("cx", function(d) { return x(d[dimensions[0]]); })
                .attr("cy", function(d) { return y(d[dimensions[1]]); })
                .style("fill", function(d) {
                    return color(d.cluster);
                })
                .style("stroke", function(d) { return "black"; } );
            }
            // legend stuff:
            // var legend = svg.selectAll(".legend")
            // .data(color.domain())
            // .enter().append("g")
            // .attr("class", "legend")
            // .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });
            //
            // legend.append("rect")
            // .attr("x", width - 18)
            // .attr("width", 18)
            // .attr("height", 18)
            // .style("fill", color);
            //
            // legend.append("text")
            // .attr("x", width - 24)
            // .attr("y", 9)
            // .attr("dy", ".35em")
            // .style("text-anchor", "end")
            // .text(function(d) { return d; });

            //append rect to svg so that we can click on it
            svg.append("rect")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .attr("fill", "rgba(255, 0, 0, 0)"); //transparent so that we can see it, but not "none" because that barrs clicking on it

            $scope.$on("redrawData", function() {
                var data = $scope.$ctrl.data;
                svg.selectAll(".dot")
                .style("fill", function(d) { return color(d.cluster); });
            });
        };
        //listen for broadcast telling visualizations to update (/build)
        $scope.$on("updateVisualizations", function() {
            if (dimensions == null) dimensions = d3.keys($scope.$ctrl.data[0]);
            buildSVG();
        });
        //listen for broadcast from pc chart to reorder axes, use param to determine new axis positions
        $scope.$on("reorderAxes", function(args1, args2) {
            // console.log("recieved reorder axes cmd");
            // console.log(args2);
            dimensions = args2;
            buildSVG();
        });
    }
});
