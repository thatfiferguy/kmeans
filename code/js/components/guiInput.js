angular.module("theDriver").component("guiInput",{
    template: `
    <div id="guiinputreplace">this will be replaced with the gui input</div>
    <div id="guiinputcontrols">
    <table>
    <tr>
    <td class="inputpair_label">Number of points created on click:</td>
    <td class="inputpair_input"><input type="number" name="numptsonclick" ng-model="guicontrols.numpts" min="1"></td>
    </tr>
    <tr>
    <td class="inputpair_label">Locational spread of points:</td>
    <td class="inputpair_input"><input type="number" name="numptsonclick" ng-model="guicontrols.spread" min="0"></td>
    </tr>
    </table>
    </div>`,
    bindings: { data: '=', nummeans: "=" }, //refer to with $scope.$ctrl.data
    restrict: "E",
    controller: function ($scope) {
        // inputs from gui controls
        $scope.guicontrols = {
            "numpts": 30,
            "spread": 1
        };

        clusternumber = 0; //on each mouse click, increase by 1. Is the cluster ID for the next click point set

        var buildSVG = function() {
            console.log("building GUI input");
            var size = Math.min(window.innerWidth, window.innerHeight);
            var margin = {top: 20, right: 20, bottom: 30, left: 40},
            // width = 500 - margin.left - margin.right,
            // height = 500 - margin.top - margin.bottom;
            width = size/1.5 - margin.left - margin.right,
            height = size/1.5 - margin.top - margin.bottom;

            var x = d3.scale.linear()
            .range([0, width]);

            var y = d3.scale.linear()
            .range([height, 0]);

            var color = d3.scale.category20();

            var xAxis = d3.svg.axis()
            .scale(x)
            .tickSize(1)
            .ticks(4) //number of markings
            .orient("bottom");

            var yAxis = d3.svg.axis()
            .scale(y)
            .tickSize(1)
            .ticks(4) //number of markings
            .orient("left");

            // remove previous SVG or content
            document.getElementById("guiinputreplace").innerHTML = "";

            var svg = d3.select("#guiinputreplace").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            // get new copy of data
            var data = $scope.$ctrl.data;

            x.domain([-10,10]).nice();
            y.domain([-10,10]).nice();

            svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .append("text")
            .attr("class", "label")
            .attr("x", width)
            .attr("y", -6)
            .style("text-anchor", "end");

            svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end");

            svg.selectAll(".dot")
            .data($scope.$ctrl.data)
            .enter().append("circle")
            .attr("class", "dot")
            .attr("r", 3.5)
            .style('opacity', 0.5)
            .attr("cx", function(d) { return x(d.x); })
            .attr("cy", function(d) { return y(d.y); })
            .style("fill", function(d) { return color(d.clusterRef); });

            // legend stuff
            // var legend = svg.selectAll(".legend")
            // .data(color.domain())
            // .enter().append("g")
            // .attr("class", "legend")
            // .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });
            //
            // legend.append("rect")
            // .attr("x", width - 18)
            // .attr("width", 18)
            // .attr("height", 18)
            // .style("fill", color);
            //
            // legend.append("text")
            // .attr("x", width - 24)
            // .attr("y", 9)
            // .attr("dy", ".35em")
            // .style("text-anchor", "end")
            // .text(function(d) { return d; });

            //append rect to svg so that we can click on it
            svg.append("rect")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .attr("fill", "rgba(255, 0, 0, 0)"); //transparent so that we can see it, but not "none" because that barrs clicking on it

            // On mouse click add a point to the dataset
            // via http://bl.ocks.org/WilliamQLiu/76ae20060e19bf42d774
            svg.on("click", function() {
                //TODO more points on click!
                var coords = d3.mouse(this);

                //guicontrols are (for reference):
                // $scope.guicontrols = {
                //     "numpts": 30,
                //     "spread": 30
                // };

                /**
                 * Return a random number in a gaussian distribution between 0 and 1.
                 * @return {float} between -1 and 1
                 */
                function gaussianRand() {
                    var rand = 0;

                    for (var i = 0; i < 6; i += 1) {
                        rand += Math.random();
                    }

                    return ((rand / 6) - 0.5) * 2;
                }

                for (var i = 0; i < $scope.guicontrols.numpts; i++) {



                    //add a new point to the dataset
                    // choose random x and y coordinate
                    var point = {
                        x: (gaussianRand() * $scope.guicontrols.spread) + x.invert(coords[0]),
                        y: (gaussianRand() * $scope.guicontrols.spread) + y.invert(coords[1]),
                        clusterRef: clusternumber,
                        cluster: 0 //this will be reassigned later
                    };
                    $scope.$ctrl.data.push(point);
                }

                clusternumber++;
                $scope.$apply(function() {
                    $scope.$ctrl.nummeans.cnt++;
                });

                //update visualization with new data
                svg.selectAll(".dot")
                .data($scope.$ctrl.data)
                .enter().append("circle")
                .attr("class", "dot")
                .style('opacity', 0.5)
                .attr("r", 3.5)
                .attr("cx", function(d) { return x(d.x); })
                .attr("cy", function(d) { return y(d.y); })
                .style("fill", function(d) { return color(d.clusterRef); });
            });
        };


        buildSVG();
    } // End controller
});
