angular.module("theDriver").component("parallelCoordinates",{
    template: '<span id="parallelcoordinatesreplace">this will be replaced with the parallel coordinates plot</span>',
    bindings: {
            data: '=', //the data
            means: "=", //the means
            fields: "=" //the fields to display
        }, //refer to with $scope.$ctrl.data    restrict: "E",
    controller: function ($scope) {
        var dimensions;
        var buildSVG = function() {
            console.log("Building PC chart");


            var margin = {top: 30, right: 10, bottom: 10, left: 10},
            width = 960 - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;

            var x = d3.scale.ordinal().rangePoints([0, width], 1),
            y = {},
            dragging = {};

            // var line = d3.svg.line(),
            var line = d3.svg.line()
                .defined(function(d) { return !isNaN(d[1]); });

            var axis = d3.svg.axis().orient("left"),
            background,
            foreground;

            var color = d3.scale.category20();

            document.getElementById("parallelcoordinatesreplace").innerHTML = "";

            var svg = d3.select("#parallelcoordinatesreplace").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            var data = $scope.$ctrl.data;

            // if (dimensions === null) {
            //     dimensions = $scope.$ctrl.fields.filter(function(d) {
            //         return d != "name" && (y[d] = d3.scale.linear()
            //         .domain(d3.extent(data, function(p) { return +p[d]; }))
            //         .range([height, 0]));
            //     });
            // }

            // build dimensons
            if (dimensions == null) {
                // dimensions = $scope.$ctrl.fields;
                dimensions = d3.keys(data[0]);
                // dimensions.push("cluster"); //also display cluster as a field.
            }

            var categoricalDims = new Set();
            data.forEach(function(datum) {
                d3.keys(data[0]).forEach(function(dim) {
                    if (isNaN(datum[dim])) categoricalDims.add(dim);
                });
            });

            dimensions.forEach(function(d) {
                y[d] = categoricalDims.has(d) ?
                d3.scale.ordinal()
                .domain(data.map(function(datum) {return datum[d]; }).sort())
                .rangePoints([0, height])
                :
                d3.scale.linear()
                .domain(d3.extent(data, function(p) { return +p[d]; }))
                .range([height, 0]);
            });

            // Extract the list of dimensions and create a scale for each.
            x.domain(dimensions);


            // Add grey background lines for context.
            background = svg.append("g")
            .attr("class", "background")
            .selectAll("path")
            .data(data)
            .enter().append("path")
            .attr("d", path);


            // Add blue foreground lines for focus.
            foreground = svg.append("g")
            .attr("class", "foreground")
            .selectAll("path")
            .data(data)
            .enter().append("path")
            .style("stroke", function(d) {
                return color(d.cluster);
            })
            .attr("d", path);

            meanline = svg.append("g")
            .attr("class", "meanline")
            .selectAll("path")
            .data($scope.$ctrl.means)
            .enter().append("path")
            .style("stroke", "black")
            .style("stroke-width", 2).attr("d", path);


            // Add a group element for each dimension.
            var g = svg.selectAll(".dimension")
            .data(dimensions)
            .enter().append("g")
            .attr("class", "dimension")
            .attr("transform", function(d) { return "translate(" + x(d) + ")"; })
            .call(d3.behavior.drag()
            .origin(function(d) { return {x: x(d)}; })
            .on("dragstart", function(d) {
                dragging[d] = x(d);
                background.attr("visibility", "hidden");
            })
            .on("drag", function(d) {
                dragging[d] = Math.min(width, Math.max(0, d3.event.x));
                foreground.attr("d", path);
                meanline.attr("d", path);
                dimensions.sort(function(a, b) { return position(a) - position(b); });
                x.domain(dimensions);
                g.attr("transform", function(d) { return "translate(" + position(d) + ")"; });
            })
            .on("dragend", function(d) {
                delete dragging[d];
                transition(d3.select(this)).attr("transform", "translate(" + x(d) + ")");
                transition(foreground).attr("d", path);
                transition(meanline).attr("d", path);

                background
                .attr("d", path)
                .transition()
                .delay(500)
                .duration(0)
                .attr("visibility", null);
                $scope.$root.$broadcast("reorderAxes", dimensions); //tell scatterplot to reorder its axes according to the param we are sending
            }));


            var clusterCnt = d3.max(data, function(d) {return d.cluster; }); //number of clusters
            var clusterRefCnt = d3.max(data, function(d) {return d.clusterRef; }); //number of clusterRefs

            // Add an axis and title.
            g.append("g")
            .attr("class", "axis")
            .each(function(d) {
                //format cluster dimension better
                if(d === "cluster") {
                    d3.select(this).call(axis.scale(y[d]).tickFormat(d3.format("d")).ticks(clusterCnt));
                } else if (d === "clusterRef") {
                    d3.select(this).call(axis.scale(y[d]).tickFormat(d3.format("d")).ticks(clusterRefCnt));
                } else {
                    d3.select(this).call(axis.scale(y[d]));
                }
            })
            .append("text")
            .style("text-anchor", "middle")
            .attr("y", -9)
            .text(function(d) { return d; });

            // Add and store a brush for each axis.
            g.append("g")
            .attr("class", "brush")
            .each(function(d) {
                d3.select(this).call(y[d].brush = d3.svg.brush().y(y[d]).on("brushstart", brushstart).on("brush", brush));
            })
            .selectAll("rect")
            .attr("x", -8)
            .attr("width", 16);


            function position(d) {
                var v = dragging[d];
                return v == null ? x(d) : v; //ignore errors here about === vs ==: === doesn't work!
            }

            function transition(g) {
                return g.transition().duration(500);
            }

            // Returns the path for a given data point.
            function path(d) {
                thepath = dimensions.map(function(p) {
                    return [position(p), y[p](d[p])];
                });
                if (d.isCluster) console.log(thepath);

                return line(thepath);
            }

            function brushstart() {
                d3.event.sourceEvent.stopPropagation();
            }

            // Handles a brush event, toggling the display of foreground lines.
            function brush() {
                var actives = dimensions.filter(function(p) { return !y[p].brush.empty(); }),
                extents = actives.map(function(p) { return y[p].brush.extent(); });
                foreground.style("display", function(d) {
                    return actives.every(function(p, i) {
                        return extents[i][0] <= d[p] && d[p] <= extents[i][1];
                    }) ? null : "none";
                });
            }

            $scope.$on("redrawData", function() {
                var data = $scope.$ctrl.data;

                svg.selectAll(".foreground")
                .selectAll("path")
                .style("stroke", function(d) {
                    return color(d.cluster);
                })
                .attr("d", path);
            });
        };



        $scope.$on("updateVisualizations", function() { //listen for broadcast telling visualizations to update (/build)
            buildSVG();
        });
    } //end controller
});
