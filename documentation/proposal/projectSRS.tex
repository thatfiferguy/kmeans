
\documentclass{article}
\usepackage{graphicx} %pics
% \usepackage{showframe}
\usepackage{fancyhdr}
\usepackage{enumitem} % noitemsep
\usepackage{amsmath, amsfonts, amsthm, amssymb, algorithm, graphicx, mathtools,xfrac}
\usepackage{listings}
\usepackage{titling}




% hyperlinks
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,
    urlcolor=blue,
}
\usepackage{marginnote} % notes in margins

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{invariant}[theorem]{Invariant}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{property}[theorem]{Property}
\newtheorem{proposition}[theorem]{Proposition}

% \renewcommand{\thesubsection}{section\arabic{subsection}} % subsection as (a) or (b)...
% \renewcommand{\thesection}{\arabic{section}.} % section as 1. or 2.


% \usepackage{fullpage} % margins


\pagestyle{fancy}
% \renewcommand{\headrulewidth}{0pt} % no header line


\title{CS201: Project Proposal}
\author{Josh Trahan}
\date{\today }

\lhead{\thedate}
\chead{\thetitle}
\rhead{\theauthor}


\begin{document}

\maketitle
\tableofcontents
\pagebreak

\section{Introduction}
\subsection{Overview}

The purpose of this project is to implement a suite of tools demonstrating the operation of the K-Means algorithm. The end-goal features of this project are as follows:

\begin{itemize}
    \item A choice of methodology of inputting data: either reading data from a CSV (see \S \ref{csvinput}), or inputting data via a graphical user interface similar to a scatterplot (see \S \ref{graphicalinput}).
    \item An implementation of the standard K-means algorithm allowing for an adaptive number of dimensions will categorize input data based on a user-inputted number of means. See \S \ref{kmeansimplementation}.
    \item A set of visualizations which show the resulting categorization, namely a scatterplot and a \href{https://bl.ocks.org/mbostock/1341021}{parallel-coordinates chart}, linked in a way such that the first two dimensions of the parallel-coordinates chart define the {\em x} and {\em y} axes of the scatterplot respectively. This will allow $>2$ dimensional data to be represented to users on a 2 dimensional screen.
\end{itemize}

\subsection{Technology}

This project will be implemented in html/javascript, using the {\em AngularJS} and {\em D3.js} libraries. I choose this technology for the implementation because:

\begin{enumerate}
    \item {\em D3.js} facilitates an easy display of data, with a wide range of skeleton code available.
    \item {\em AngularJS} facilitates easy synchronization of data between the modules (input, calculation, representation) of the project.
    \item Both of the above technologies are javascript libraries. Using html to display the project as a webpage facilitates easy deployment, and means I can easily display it in my portfolio of personal projects on my resumé by deploying it publicly on the web.
    \item I have significant experience with all of these technologies in combination, including specifically the connection of the parallel-coordinates and scatterplot visualizations.
\end{enumerate}

\section{Input} \label{input}

A simple user binary-choice interface will allow the user to select one of the two following input methods. Once one is selected, the other will be hidden from the view.

\subsection{From a CSV}\label{csvinput}

A button on the webpage will allow the user to upload a CSV file of features to categorize. I will use the skeleton code \href{http://bl.ocks.org/syntagmatic/raw/3299303/}{here} as a basis for this functionality.

Once uploaded, a drop-down menu will be populated with a list of ``dimensions'' from the data. This menu will be used to select the ``clustering reference value'' for determining accuracy. After K-Means assigns the calculated ``cluster value'' we can determine accuracy by checking the homogeneity of ``cluster values'' inside of each ``clustering reference value'' set -- in other words, optimally each set of features categorized by ``cluster value'' will have the same ``cluster reference value'' in common with one another.

Often times I would expect one would chose the clustering reference value as a nominal field -- for example, with the \href{https://archive.ics.uci.edu/ml/datasets/Iris}{iris dataset}, one would probably choose ``class'' (a.k.a flower type) as the clustering reference value. The clustering reference value chosen will not be used in the k-means calculation step, so if it is numerical it will not pollute the data.

\subsection{From a GUI}\label{graphicalinput}

A scatterplot-like GUI will be implemented to facilitate an alternative method for creating features, albeit simpler features than the CSV input allows. Operation of this GUI is defined as follows:

\begin{enumerate}
    \item The user will be presented with an initially blank input area.
    \item Three numeric controls are adjacent to the input area:
        \begin{itemize}
            \item number of points to add on click.
            \item Locational spread of points to add on click \label{locpts}
        \end{itemize}
    \item Upon clicking on the input area, a number of points will be created centered on the location the user clicked. The quantity of points pulled from the input of the same, and will be locationally centered at the click-point but spread in a gaussian distribution around the click point. The spread can be made larger or smaller using the locational spread control.
    \item On each click, the a number-of-means counter will increment, and the points created on that click will be auto-assigned a new unique ``cluster reference value.'' This will facilitate accuracy testing later on -- though if one places overlapping clusters the validity of such a value is nullified.
\end{enumerate}

This control only facilitates the creation of clusters in two dimensions.


\section{K-Means algorithm}\label{kmeansimplementation}

After inputting data, the user will select a number of means value from a numeric input field (which may be pre-populated if the user used the graphical input -- see \S \ref{graphicalinput}) and press the ``cluster'' button. At this point the K-Means algorithm will run on the loaded data. The load data panel (\ref{input}) will hide, and the visualization(s) panel (\S \ref{visualizations}) will display.

The algorithm will ignore any non numerical fields in the data (if loaded from a CSV -- data generated from the GUI input will not have any non-numeric fields) as well as the ``cluster reference value'' field when calculating the means.

The K-Means algorithm will assign a value to a new cluster field for each feature. From there, the visualizations panel (\S \ref{visualizations}) will display the features and categorize them by the value of this cluster field.

\section{Visualization} \label{visualizations}

After running the K-Means algorithm on the data we will open a pair of panels showing the scatterplot (based on skeleton code \href{https://bl.ocks.org/mbostock/3887118}{here}) and parallel coordinates plot (based on skeleton code \href{https://bl.ocks.org/mbostock/1341021}{here}). The behavior of these individual visualizations is described in the following sections.

\subsection{Scatterplot} \label{scatterplot}

The scatterplot behaves about how one would expect - displaying features in 2-dimensional space. A toggle button adjacent to the visualization will change the format of the display: the color of the points will be either derived from the cluster found using the K-means algorithm or the one found via the input method (the ``cluster reference value'') depending on the state of the toggle button.

One nifty feature of the scatterplot is that the {\em x} and {\em y} dimensions are derived from the order of the dimensions in the parallel coordinates plot. The leftmost dimension will be used as the {\em x} coordinate, and the dimension to the right of that will be used as the {\em y} coordinate. The user can reorder these dimensions on the PC chart and the scatterplot will update live.

\subsection{Parallel-coordinates plot}

The parallel coordinates plot is a little-known visualization which is designed in such a way to facilitate viewing features in a large number of dimensions simultaneously. The chart is shown as a number of vertical bars (each one representing a dimension of the data). Between these bars are spanned lines - each line is one feature, and where it falls on each dimension is the value of that feature at that dimension.

The parallel coordinates chart allows for a number of features:

\begin{itemize}
    \item Dimensions can be ``brushed'' -- selecting all features inside of a given range in the dimension. This is only a visual feature and is only useful for highlighting portions of the data during analysis of the graph.
    \item Dimensions can be reordered. This feature is linked to the scatterplot, such that the first and second dimensions of the parallel-coordinates chart define the {\em x} and {\em y} dimensions of the scatterplot.
    \item Like the scatterplot, the color-coding of the features will be based on either the ``cluster reference value'' or K-Means assigned cluster of the feature, depending on the value of the display toggle described in \S \ref{scatterplot}.
\end{itemize}

\section{Progress steps}

\begin{enumerate}
    \item Construct a basic framework of the website, where pressing the appropriate buttons to move along the stages (input $\rightarrow$ calculation $\rightarrow$ visualization) correctly shows and hides the relevant displays. Create all of the controls without any backing functionality.
    \item Construct the graphical input area and scatterplot -- use the scatterplot to display the features generated from the graphical input area and their ``cluster reference value''.
    \item Implement the K-Means algorithm. Modify scatterplot to display the cluster from K-means, and implement the display-toggle feature. \label{reasonableprogress}
    \item Implement the CSV input feature.
    \item Implement the parallel coordinates chart.
    \item Bind the order of dimensions of the parallel-coordinates chart to the {\em x} and {\em y} axes of the scatterplot.
\end{enumerate}

This is a large project, and obviously it is possible I won't complete all of the features. At the very least I intend to complete up to item \ref{reasonableprogress} on the above list.

\end{document}
